using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private StateMachine _fsm;
    [SerializeField] private Transform _spawnCard1;
    [SerializeField] private Transform _spawnCard2;

    private void Awake()
    {
        _fsm = GetComponent<StateMachine>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Card card = collision.gameObject.GetComponent<Card>();

        if (!_fsm.PlayerStats.IsMyTurn)
        {
            TableManager.Instance.Deck.ReinsertCard(card);
            return;
        }

        if (card != null)
            _fsm.PlayerStats.AddPlayerCard(card);
    }

    public void PlaceCards(Card card1, Card card2)
    {
        var cardObject1 = Instantiate(card1.gameObject, _spawnCard1.position,
            Quaternion.AngleAxis(180, transform.forward));
        var cardObject2 = Instantiate(card2.gameObject, _spawnCard2.position,
            Quaternion.AngleAxis(180, transform.forward));
        cardObject1.transform.rotation *= Quaternion.FromToRotation(cardObject1.transform.forward, transform.forward);
        cardObject2.transform.rotation *= Quaternion.FromToRotation(cardObject2.transform.forward, transform.forward);
    }
}
