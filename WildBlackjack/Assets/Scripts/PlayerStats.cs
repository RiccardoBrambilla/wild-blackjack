﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    private List<Card> _cards = new List<Card>(); 
    private bool _isMyTurn = false;
    private bool _busted = false;
    private bool _blackjack = false;

    public System.Action<bool> playerReceivedCardAction;
    public System.Action<bool> dealerReceivedCardAction;

    public List<Card> Cards { get => _cards; set => _cards = value; }
    public int TotalValue => CalculateValueOfCards();
    public bool IsMyTurn { get => _isMyTurn; set => _isMyTurn = value; }

    public bool Busted { get => _busted; set => _busted = value; }
    public bool Blackjack { get => _blackjack; set => _blackjack = value; }

    public (Card, Card) StartingCards()
    {
        (Card, Card) tuplaStartingCards = (_cards[0], _cards[1]);
        return tuplaStartingCards;
    }

    public void FlipStartingCards()
    {
        for(int i = 0; i < _cards.Count; ++i)
        {
            _cards[i].Flip();
        }
        if (TotalValue == 21) //This is not the correct place to put this
            _blackjack = true;
    }

    public int CalculateValueOfCards()
    {
        int totalValue = 0;
        int numberOfAces = 0;
        foreach (Card card in _cards)
        {
            totalValue += card.Rank;
            if (card.Rank == 11)
                numberOfAces++;
        }

        if (totalValue <= 21)
            return totalValue;

        for (int i = 0; i < numberOfAces; i++) //Count aces in the value
        {
            totalValue -= 10;
                if (totalValue <= 21)
                    return totalValue;
        }
        return totalValue;
    }
    public PlayerStats(Card card1, Card card2)
    {
        _cards.Add(card1);
        _cards.Add(card2);
    }

    public PlayerStats(Card card1, Card card2, bool Turn)
    {
        _cards.Add(card1);
        _cards.Add(card2);
        _isMyTurn = Turn;
    }

    public void AddPlayerCard(Card card)
    {
        _cards.Add(card);
        playerReceivedCardAction?.Invoke(true);
    }

    public void AddDealerCard(Card card)
    {
        _cards.Add(card);
        dealerReceivedCardAction?.Invoke(true);
    }
}
