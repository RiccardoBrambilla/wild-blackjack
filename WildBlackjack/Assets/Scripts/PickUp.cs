using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    private float _distance;
    private Rigidbody _rb;
    private Vector3 _currVel = new Vector3();

    [SerializeField] private InputManager _gameManager;
    [SerializeField] private bool _isHolding = false;
    [SerializeField] private GameObject _item;
    [SerializeField] private GameObject _handObject;

    public float m_FollowSmoothTime = 0.05f;
    public float m_FloatToTargetSpeed;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _item = gameObject;
        _handObject = GameObject.FindGameObjectWithTag("Hand");
        _gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InputManager>();
    }

    private void Update()
    {
        _distance = Vector3.Distance(_item.transform.position, _handObject.transform.position);
        if (_distance >= 31f)        //Drop if item is too far
        {
            _isHolding = false;
        }

        if (_isHolding)
        {
            if (Input.GetMouseButtonDown(2)) //Throw object
            {
                _item.GetComponent<Rigidbody>().useGravity = true;
                _item.GetComponent<Rigidbody>().AddForce(_handObject.transform.forward * _gameManager.m_ThrowForce, ForceMode.Impulse);
                _isHolding = false;
            }

            if (Input.GetMouseButtonDown(1)) //Drop object
            {
                _item.GetComponent<Rigidbody>().useGravity = true;
                _isHolding = false;
            }
        }   
    }
    private void FixedUpdate()
    {
        if (_isHolding)
        {
            _rb.velocity = Vector3.SmoothDamp(_rb.velocity, (_handObject.transform.position - transform.position) * m_FloatToTargetSpeed,
                ref _currVel, m_FollowSmoothTime);
            _rb.angularVelocity = Vector3.zero;
        }
    }

    private void OnMouseDown()
    {
        if (_distance <= 30f && !_isHolding)
            GrabObject();
    }

    public void GrabObject()
    {
        _isHolding = true;
        _item.GetComponent<Rigidbody>().useGravity = false;
        _item.GetComponent<Rigidbody>().detectCollisions = true;
    }


}
