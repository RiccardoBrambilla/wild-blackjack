﻿using UnityEngine;
[System.Serializable]
public enum SuitEnum
{
    Hearts,
    Clubs,
    Diamonds,
    Spades,
}
public class Card : MonoBehaviour
{
    private bool _isFlipped = false;
    private bool _isOnTheTable = true;
    [SerializeField] private SuitEnum _suit;
    [SerializeField] private int _rank;

    public bool IsOnTheTable { get => _isOnTheTable; set => _isOnTheTable = value; }

    public SuitEnum Suit => _suit;
    public int Rank => _rank;

    public Card(SuitEnum suit, int rank)
    {
        this._suit = suit;
        this._rank = rank;
    }

    [ContextMenu("Flip card")]
    public void Flip()
    {
        transform.Rotate(transform.forward, 180f);
    }

    private void OnCollisionEnter(Collision collision) 
    {
        StateMachine fsm = collision.gameObject.GetComponent<StateMachine>();

        if (fsm == null)
            return;

        if (fsm.PlayerType == WildBlackjack.MyUtility.StateMachineType.Dealer && !fsm.PlayerStats.IsMyTurn)
        {
            TableManager.Instance.Deck.ReinsertCard(this);
            return;
        }    

        gameObject.GetComponent<Rigidbody>().isKinematic = true;

        if(!_isFlipped)
            Flip();
    }
}
