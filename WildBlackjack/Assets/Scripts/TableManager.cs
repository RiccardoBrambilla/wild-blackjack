using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class TableManager : MonoBehaviour
{
    private static TableManager _instance;
    [SerializeField] private int _nOfPlayers = 3;
    [SerializeField] private Deck _deck;
    [SerializeField] private StateMachine _dealerStateMachine;
    [SerializeField] private List<StateMachine> _playersStateMachines = new List<StateMachine>();
    public static TableManager Instance => _instance;
    public StateMachine DealerMachine { get => _dealerStateMachine; set => _dealerStateMachine = value; }
    public List<StateMachine> StateMachine { get => _playersStateMachines; }
    public Deck Deck { get => _deck; set => _deck = value; }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        RoundStart(false);
    }

    private void ActivateDealerMachine()
    {
        PlayerStats playerStats = new PlayerStats(_deck.TakeCard(), _deck.TakeCard(), false);
        _dealerStateMachine.SetPlayerInfo(playerStats, WildBlackjack.MyUtility.StateMachineType.Dealer);
    }

    private void ActivatePlayerMachines(int numberOfPlayers)
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            _playersStateMachines[i].gameObject.SetActive(true);
            PlayerStats playerStats = new PlayerStats(_deck.TakeCard(), _deck.TakeCard(), true);
            _playersStateMachines[i].SetPlayerInfo(playerStats, WildBlackjack.MyUtility.StateMachineType.Player);
        }
    }
    public void RoundStart(bool resetStates) //Setup deck, activate fsm's and can reset states of fsm's
    {
        _deck.PrepareDeck();    
        ActivateDealerMachine();  
        ActivatePlayerMachines(_nOfPlayers);  
        if (resetStates)
        {
            foreach (StateMachine v in _playersStateMachines) 
                v.ResetState();
            _dealerStateMachine.ResetState();
        }
    }

    public bool CheckIfAllPlayersFinished() //Check if all players ended the turn
    {
        int i = 0;
        for (int p = 0; p < _nOfPlayers; p++)
            if (_playersStateMachines[p].m_CurrentState is OpponentTurn) i++;
        if (i == _nOfPlayers) return true;
        else return false;
    }

    public void CheckWhoWins()
    {
        if (_dealerStateMachine.PlayerStats.Busted) //If dealer bust
        {
            _dealerStateMachine.m_currentStateText.SetText("You bust!");
            for (int p = 0; p < _nOfPlayers; p++)
                if (!_playersStateMachines[p].PlayerStats.Busted) //For each player that have not busted give them a win
                    _playersStateMachines[p].m_currentStateText.SetText("Win!!");
            return;
        }

        if (_dealerStateMachine.PlayerStats.Blackjack) //If dealer blackjack
        {
            _dealerStateMachine.m_currentStateText.SetText("Blackjack!!");
            for (int p = 0; p < _nOfPlayers; p++)
                if (_playersStateMachines[p].PlayerStats.Blackjack) //For each player that have a blackjack, its draw
                    _playersStateMachines[p].m_currentStateText.SetText("Draw :|");
                else
                    _playersStateMachines[p].m_currentStateText.SetText("Lost :(");
            return;
        }

        for (int p = 0; p < _nOfPlayers; p++) //Check who is higher, lower or equal to the dealer
        {
            if (_playersStateMachines[p].PlayerStats.TotalValue > _dealerStateMachine.PlayerStats.TotalValue)
                _playersStateMachines[p].m_currentStateText.SetText("Win!!");
            else if (_playersStateMachines[p].PlayerStats.TotalValue < _dealerStateMachine.PlayerStats.TotalValue)
                _playersStateMachines[p].m_currentStateText.SetText("Lost :(");
            else
                _playersStateMachines[p].m_currentStateText.SetText("Draw :|");
        }
    }

    #region Unused Methods
    public void AddPlayerToGame(StateMachine stateMachine)
    {
        if (_nOfPlayers < 3)
            _playersStateMachines.Add(stateMachine);
    }
    public void RemovePlayerFromTheGame()
    {
        if (_nOfPlayers > 1)
            _playersStateMachines.RemoveAt(_nOfPlayers);
    }
    #endregion
}
