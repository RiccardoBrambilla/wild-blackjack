using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float _currXRot;
    private float _xRotVel;
    private float _currYRot;
    private float _yRotVel;
    private float _xRot;
    private float _yRot;

    public float m_SmoothAmount;
    public float m_MouseSensitivityX;
    public float m_MouseSensitivityY;
    public float m_MaxVerticalRot;
    public float m_MinVerticalRot;

    // Start is called before the first frame update
    void Start()
    {
        //Show/lock Cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
        _xRot = transform.eulerAngles.y;
        _currXRot = _xRot;
    }

    // Update is called once per frame
    void Update()
    {
        //Mouse rotation

        _xRot += Input.GetAxisRaw("Mouse X") * m_MouseSensitivityX;
        _yRot += Input.GetAxisRaw("Mouse Y") * m_MouseSensitivityY * -1;
        _yRot = Mathf.Clamp(_yRot, m_MinVerticalRot, m_MaxVerticalRot);

    }

    private void FixedUpdate()
    {
        //Smooth Rotation

        _currXRot = Mathf.SmoothDamp(_currXRot, _xRot, ref _xRotVel, m_SmoothAmount);
        _currYRot = Mathf.SmoothDamp(_currYRot, _yRot, ref _yRotVel, m_SmoothAmount);

        transform.rotation = Quaternion.Euler(new Vector3(_currYRot, _currXRot, transform.rotation.z));
 
    }
}
