namespace WildBlackjack
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        void Redo();

        string DebugInfos();
        void ShowValue();

    }

    public static class MyUtility
    {
        public enum Seed
        {
            Club,
            Spades,
            Hearts,
            Diamonds
        }

        public enum StateMachineType
        {
            Dealer,
            Player
        }
    }
}