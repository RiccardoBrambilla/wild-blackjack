using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Deck : MonoBehaviour
{
    private List<Card> _remainingCards = new List<Card>();
    private List<Card> _extractedCards = new List<Card>();
    [SerializeField] private DeckSO _deckSO;
    [SerializeField] private Transform spawnCardTransform;

    private void OnMouseDown()
    {
        Instantiate(TakeCard(), spawnCardTransform.position, transform.rotation);
    }

    private List<Card> CardsOnTheTable()
    {
        List<Card> extractedCardsOnTable = _extractedCards.Where(x => x.IsOnTheTable = true).ToList(); ;
        return extractedCardsOnTable;
    }

    private void CleanExtractedCards(List<Card> extractedCardsOnTable)
    {
        extractedCardsOnTable.ForEach(z => _extractedCards.Remove(z));
    }

    public void PrepareDeck()
    {
        _deckSO.m_CardDatabase.ForEach(x => _remainingCards.Add(x));
    }

    public void PutCardsUnderTheDeck(bool reverse) //Needs to be fixed
    {
        List<Card> extractedCardsOnTable = CardsOnTheTable();
        //extractedCardsOnTable.ForEach(x => remainingCards.Add(x));
        //CleanExtractedCards(extractedCardsOnTable);

        if (reverse) //Optional
            extractedCardsOnTable.Reverse();

        extractedCardsOnTable.ForEach(x => x.gameObject.SetActive(false)); //Doesn't work
        extractedCardsOnTable.ForEach(x => ReinsertCard(x));
    }

    public void ReinsertCard(Card card)
    {
        _extractedCards.Remove(card);
        _remainingCards.Add(card);
    }

    public Card TakeCard()
    {
        Card card = _remainingCards[0];

        if (card.gameObject.activeSelf == false)
            card.gameObject.SetActive(true);

        _extractedCards.Add(card);
        _remainingCards.RemoveAt(0);

        return card;
    }

    public void ShuffleDeck()
    {
        System.Random random = new System.Random();
        int index = _remainingCards.Count;
        while (index > 1)
        {
            int k = random.Next(index);
            --index;

            Card temp = _remainingCards[k];
            _remainingCards[k] = _remainingCards[index];
            _remainingCards[index] = temp;
        }
    }
}
