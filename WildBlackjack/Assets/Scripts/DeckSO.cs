using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DeckSO", menuName = "ScriptableObject/Deck")]
public class DeckSO : ScriptableObject
{
    public List<Card> m_CardDatabase;

    public Card TakeCard()
    {
        Card card = m_CardDatabase[0];
        m_CardDatabase.RemoveAt(0);

        return card;
    }
}

public static class ScriptableObjectExtension // Creates and returns a clone of any given scriptable object.
{
    public static T Clone<T>(this T scriptableObject) where T : ScriptableObject
    {
        if (scriptableObject == null)
        {
            Debug.LogError($"ScriptableObject was null. Returning default {typeof(T)} object.");
            return (T)ScriptableObject.CreateInstance(typeof(T));
        }

        T instance = Object.Instantiate(scriptableObject);
        instance.name = scriptableObject.name; // remove (Clone) from name
        return instance;
    }
}
