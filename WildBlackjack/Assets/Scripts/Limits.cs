using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limits : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Card card = collision.gameObject.GetComponent<Card>();
        if (card != null)
            card.IsOnTheTable = false;
    }
}

