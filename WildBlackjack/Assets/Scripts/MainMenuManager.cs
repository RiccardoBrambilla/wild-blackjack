using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    private int _numberOfPlayers = 3;
    private static MainMenuManager _instance;

    public Slider m_Slider;
    public static MainMenuManager Instance => _instance;

    public int NumberOfPlayers => _numberOfPlayers;

    private void Awake()
    {
        if (_instance == null)          
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this.gameObject);
        ShowCursor(true);
    }

    private void ShowCursor(bool status)
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = status;
    }

    public void OnPlayersNumberChange()
    {
        _numberOfPlayers = (int)m_Slider.value;
        Debug.Log(_numberOfPlayers);
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

