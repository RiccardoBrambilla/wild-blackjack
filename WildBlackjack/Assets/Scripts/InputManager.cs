using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    public Text m_ThrowForceText;
    public float m_ThrowForce = 10f;
    [SerializeField] private float maxThrowForce = 300f;
    [SerializeField] private float minThrowForce = 5f;

    void Start()
    {
        m_ThrowForceText.text = m_ThrowForce.ToString();
    }

    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            if (m_ThrowForce < maxThrowForce)
            {
                m_ThrowForce += 5f;
                m_ThrowForceText.text = m_ThrowForce.ToString();
            }
        }

        if (Input.GetKeyDown("q"))
        {
            if (m_ThrowForce > minThrowForce)
            {
                m_ThrowForce -= 5f;
                m_ThrowForceText.text = m_ThrowForce.ToString();
            }
        }

        if (Input.GetKey("z") && TableManager.Instance.DealerMachine.m_CurrentState is EndGameState) 
        {
            TableManager.Instance.RoundStart(true);
        }

        if (Input.GetKeyDown("l"))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown("c"))
        {
            TableManager.Instance.Deck.ShuffleDeck();
        }
    }
}
