﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
public abstract class State : IState
{
    protected PlayerStats _playerStats;
    protected StateMachine _fsm;
    public State(StateMachine fsm, PlayerStats playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public abstract void OnEnter();


    public abstract void OnExit();


    public abstract void Tick();

}

public interface IState
{
    public void OnEnter();


    public void Tick();

    public void OnExit();
}
