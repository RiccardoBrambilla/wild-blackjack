using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StateMachine : MonoBehaviour
{
    private WildBlackjack.MyUtility.StateMachineType _playerType;
    private IState _previousState;
    private PlayerStats _playerStats; 
    [SerializeField] private Player _player;

    public int m_HoldThreshold = 14;
    public IState m_CurrentState;
    public TextMeshPro m_currentStateText;
    public PlayerStats PlayerStats => _playerStats;
    public WildBlackjack.MyUtility.StateMachineType PlayerType { get => _playerType; set => _playerType = value; }

    private void Update()
    {
        m_CurrentState.Tick();
    }

    public void SetPlayerInfo(PlayerStats playerStats, WildBlackjack.MyUtility.StateMachineType playerType)
    {
        _playerStats = playerStats;
        PlayerType = playerType;
        var startingCards = _playerStats.StartingCards();
        _player.PlaceCards(startingCards.Item1, startingCards.Item2);
        StartMachine(playerStats);
    }
    public void StartMachine(PlayerStats playerStats)
    {
        m_CurrentState = new DefaultState(this, playerStats);
        _previousState = m_CurrentState;
        m_CurrentState.OnEnter();
    }
    public void ResetState()
    {
        m_CurrentState = new DefaultState(this, _playerStats);
        SwitchToNextState(m_CurrentState);
    }

    public void SwitchToNextState(IState nextState)
    {
        if (m_CurrentState == null)
            return;

        _previousState = m_CurrentState;

        m_CurrentState.OnExit();
        m_CurrentState = nextState;
        m_CurrentState.OnEnter();
    }

    public void ReturnToPreviousState()
    {
        if (_previousState == null)
            return;

        IState tempState = m_CurrentState;
        m_CurrentState.OnExit();
        m_CurrentState = _previousState;
        _previousState = tempState;
        m_CurrentState.OnEnter();
    }
}


