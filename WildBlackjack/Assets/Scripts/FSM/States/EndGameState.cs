using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameState : State
{
    public EndGameState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        TableManager.Instance.CheckWhoWins();
        TableManager.Instance.Deck.PutCardsUnderTheDeck(false);
    }

    public override void OnExit()
    {
        
    }

    public override void Tick()
    {

    }
}
