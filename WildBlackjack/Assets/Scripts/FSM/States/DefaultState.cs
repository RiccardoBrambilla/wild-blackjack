﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;


public class DefaultState : State
{
    public DefaultState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter() //Check if the machine is a dealer or a player and change state accordingly
    {
        if (_fsm.PlayerType == WildBlackjack.MyUtility.StateMachineType.Player)
        {
            if (_playerStats.IsMyTurn)
            {
                if (_playerStats.Blackjack)
                {
                    OpponentTurn turn = new OpponentTurn(_fsm, _playerStats);
                    _fsm.SwitchToNextState(turn);
                }
                else
                {
                    CheckState turn = new CheckState(_fsm, _playerStats);
                    _fsm.SwitchToNextState(turn);
                }
            }
            else
            {
                OpponentTurn turn = new OpponentTurn(_fsm, _playerStats);
                _fsm.SwitchToNextState(turn);
            }
        }
        else if (_fsm.PlayerType == WildBlackjack.MyUtility.StateMachineType.Dealer)
        {
            //_playerStats.Cards[0].Flip(); //Not Working
            if (_playerStats.IsMyTurn)
            {
                if (_playerStats.Blackjack)
                {
                    EndGameState turn = new EndGameState(_fsm, _playerStats);
                    _fsm.SwitchToNextState(turn);
                }
                else
                {
                    DealerTurn turn = new DealerTurn(_fsm, _playerStats);
                    _fsm.SwitchToNextState(turn);
                }
            }
            else
            {
                OpponentTurn turn = new OpponentTurn(_fsm, _playerStats);
                _fsm.SwitchToNextState(turn);
            }
        }
    }

    public override void OnExit()
    {

    }

    public override void Tick()
    {

    }
}