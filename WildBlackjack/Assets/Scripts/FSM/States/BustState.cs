using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BustState : State
{
    public BustState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        _fsm.m_currentStateText.SetText("Bust");
        _playerStats.Busted = true;
        OpponentTurn turn = new OpponentTurn(_fsm, _playerStats);
        _fsm.SwitchToNextState(turn);
    }

    public override void OnExit()
    {
        
    }

    public override void Tick()
    {
        
    }
}
