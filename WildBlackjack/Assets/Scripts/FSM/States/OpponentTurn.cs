using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentTurn : State
{
    public OpponentTurn(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        _playerStats.IsMyTurn = false; //ToHack
    }
    public override void OnExit()
    {

    }

    public override void Tick()
    {
        if (_fsm.PlayerType == WildBlackjack.MyUtility.StateMachineType.Dealer)
        {
            if (TableManager.Instance.CheckIfAllPlayersFinished())
            {
                _fsm.m_currentStateText.SetText("Your turn!");
                _playerStats.IsMyTurn = true;
                _fsm.SwitchToNextState(new DefaultState(_fsm, _playerStats));
            }
        }
    }
}
