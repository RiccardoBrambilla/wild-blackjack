using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckState : State
{
    private float _waitTime = 1.3f;
    private float _elapsedTime = 0f;
    public CheckState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        _fsm.m_currentStateText.SetText("Uhmm");
    }

    public override void OnExit()
    {
        
    }

    public override void Tick()
    {
        Wait();
    }

    private void Wait() //Give a delay to AI
    {
        _elapsedTime += Time.deltaTime;
        if (_elapsedTime >= _waitTime)
        {
            CheckAction();
        }
    }

    private void CheckAction() //Check the value of the cards and switch state accordingly
    {
        Debug.LogWarning(_playerStats.TotalValue);
        if(_playerStats.TotalValue <= _fsm.m_HoldThreshold)
        {
            HitState turn = new HitState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }
        else if(_playerStats.TotalValue > _fsm.m_HoldThreshold && _playerStats.TotalValue < 22)
        {
            StandState turn = new StandState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }
        else
        {
            BustState turn = new BustState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }
    }
}
