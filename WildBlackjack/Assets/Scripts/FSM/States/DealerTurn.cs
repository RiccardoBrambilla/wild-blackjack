using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealerTurn : State
{
    private bool _hasReceivedCard = false;

    public DealerTurn(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        _fsm.m_currentStateText.SetText($"Your turn! \n {_playerStats.TotalValue}");
        _playerStats.playerReceivedCardAction += ReceivingCardStatus;
        _hasReceivedCard = false;
    }

    public override void OnExit()
    {
        _playerStats.playerReceivedCardAction -= ReceivingCardStatus;
    }

    public override void Tick()
    {
        if (Input.GetButtonDown("DealerStand"))
        {
            EndGameState turn = new EndGameState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }

        ReceivingCard();
    }

    private void ReceivingCard()
    {
        if (!_hasReceivedCard)
            return;
        _fsm.m_currentStateText.SetText($"Your turn! \n {_playerStats.TotalValue}");
        ReceivingCardStatus(false);
        if (_playerStats.TotalValue > 21)
        {
            _fsm.m_currentStateText.SetText($"You busted! \n {_playerStats.TotalValue}");
            _playerStats.Busted = true;
            EndGameState turn = new EndGameState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }

        if (_playerStats.TotalValue >= 17)
        {
            _fsm.m_currentStateText.SetText($"Round ended \n {_playerStats.TotalValue}");
            EndGameState turn = new EndGameState(_fsm, _playerStats);
            _fsm.SwitchToNextState(turn);
        }
    }
    private void ReceivingCardStatus(bool status)
    {
        _hasReceivedCard = status;
    }
}
