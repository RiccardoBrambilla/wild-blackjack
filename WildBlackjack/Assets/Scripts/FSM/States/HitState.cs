using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitState : State
{
    public HitState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    bool hasReceivedCard = false;
    public override void OnEnter()
    {
        _fsm.m_currentStateText.SetText($"Give me a card \n{_playerStats.TotalValue}");
        _playerStats.playerReceivedCardAction += ReceivingCardStatus;
        hasReceivedCard = false;
    }

    public override void OnExit()
    {
        _playerStats.playerReceivedCardAction -= ReceivingCardStatus;
    }

    public override void Tick()
    {
        ReceivingCard();
    }

    private void ReceivingCard()
    {
        if (!hasReceivedCard)
            return;

        CheckState state = new CheckState(_fsm, _playerStats);
        _fsm.SwitchToNextState(state);
    }

    private void ReceivingCardStatus(bool status)
    {
        hasReceivedCard = status;
    }
}
