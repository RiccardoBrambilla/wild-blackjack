using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandState : State
{
    public StandState(StateMachine fsm, PlayerStats playerStats) : base(fsm, playerStats)
    {
        _fsm = fsm;
        _playerStats = playerStats;
    }
    public override void OnEnter()
    {
        _fsm.m_currentStateText.SetText($"Stand with{_playerStats.TotalValue}");
        _fsm.SwitchToNextState(new OpponentTurn(_fsm, _playerStats));
    }

    public override void OnExit()
    {
     
    }

    public override void Tick()
    {
       
    }
}
